local set_uri_args = kong.service.request.set_raw_query
local DEBUG = ngx.DEBUG

-- new table
local _M = {}

function _M.add(conf)
  -- get query to add
  local query = conf.add

  -- get slug
  local slug = kong.request.get_query_arg("slug")

  kong.log(DEBUG, " ---- EXTRACTED SLUG = ", slug)

  if slug == "" or slug == nil or slug == true then
    return kong.response.exit(400, "Slug is missing")
  end

  -- build querystring
  local start = ("query=query{profile(slug:\""..slug.."\")")
  local querystring = (start .. query)
  kong.log(DEBUG, "added querystring ", querystring)
  set_uri_args(querystring)
end

return _M