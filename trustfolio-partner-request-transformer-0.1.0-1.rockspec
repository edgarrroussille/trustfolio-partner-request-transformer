package = "trustfolio-partner-request-transformer"
version = "0.1.0-1"


supported_platforms = {"linux", "macosx"}
source = {
  url = "https://edgarrroussille@bitbucket.org/edgarrroussille/trustfolio-partner-request-transformer.git",
  tag = "0.1.0"
}

description = {
  summary = "Custom plugin for Kong to allow for consumer metadata in upstream partner request.",
  homepage = "https://trustfolio.co",
  license = "Apache 2.0"
}

dependencies = {
}

local pluginName = "trustfolio-partner-request-transformer"
build = {
  type = "builtin",
  modules = {
    ["kong.plugins."..pluginName..".handler"] = "kong/plugins/"..pluginName.."/handler.lua",
    ["kong.plugins."..pluginName..".schema"] = "kong/plugins/"..pluginName.."/schema.lua",
    ["kong.plugins."..pluginName..".add_slug"] = "kong/plugins/"..pluginName.."/add_slug.lua",
  }
}